# frozen_string_literal: true

require_relative "../../../support/tracing/shared_examples"

describe Labkit::Tracing::ExternalHttp::RequestInstrumenter do
  using RSpec::Parameterized::TableSyntax

  it_behaves_like "a tracing instrumenter" do
    let(:expected_span_name) { "external_http:request" }
    let(:payload) do
      {
        method: "GET", code: "200", host: "gitlab.com", port: 80,
        path: "/gitlab-org/gitlab", scheme: "https",
      }
    end

    let(:expected_tags) do
      {
        "component" => "external_http",
        "method" => "GET",
        "code" => "200",
        "host" => "gitlab.com",
        "port" => 80,
        "path" => "/gitlab-org/gitlab",
        "scheme" => "https",
      }
    end
  end

  it_behaves_like "a tracing instrumenter" do
    let(:expected_span_name) { "external_http:request" }
    let(:payload) do
      {
        method: "POST", code: "400", host: "gitlab.com", port: 80, path: "/gitlab-org/gitlab",
        query: "abc=1&xyz=2", fragment: "normal", scheme: "https",
      }
    end

    let(:expected_tags) do
      {
        "component" => "external_http",
        "method" => "POST",
        "code" => "400",
        "host" => "gitlab.com",
        "port" => 80,
        "path" => "/gitlab-org/gitlab",
        "scheme" => "https",
      }
    end
  end

  it_behaves_like "a tracing instrumenter" do
    let(:expected_span_name) { "external_http:request" }
    let(:payload) do
      {
        method: "POST", code: "400", host: "gitlab.com", port: 80, path: "/gitlab-org/gitlab",
        scheme: "https", proxy_host: "proxy.gitlab.com", proxy_port: 8080,
      }
    end

    let(:expected_tags) do
      {
        "component" => "external_http",
        "method" => "POST",
        "code" => "400",
        "host" => "gitlab.com",
        "port" => 80,
        "path" => "/gitlab-org/gitlab",
        "scheme" => "https",
        "proxy_host" => "proxy.gitlab.com",
        "proxy_port" => 8080,
      }
    end
  end
end
