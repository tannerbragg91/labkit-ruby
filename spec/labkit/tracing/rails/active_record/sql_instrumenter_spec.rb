require_relative "../../../../support/tracing/shared_examples"

require "active_support"

describe Labkit::Tracing::Rails::ActiveRecord::SqlInstrumenter do
  using RSpec::Parameterized::TableSyntax

  where(:name, :operation_name, :exception, :connection_id, :cached, :cached_response, :sql, :expected_sql, :expected_fingerprint, :tracing_sampled) do
    nil | "active_record:sqlquery" | nil | nil | nil | false | nil | nil | nil | true
    "" | "active_record:sqlquery" | nil | nil | nil | false | nil | nil | nil | true
    "User Load" | "active_record:User Load" | nil | nil | nil | false | nil | nil | nil | true
    "Repo Load" | "active_record:Repo Load" | StandardError.new | nil | nil | false | nil | nil | nil | true
    nil | "active_record:sqlquery" | nil | 123 | nil | false | nil | nil | nil | true
    nil | "active_record:sqlquery" | nil | nil | false | false | nil | nil | nil | true
    nil | "active_record:sqlquery" | nil | nil | true | true | nil | nil | nil | true
    nil | "active_record:sqlquery" | nil | nil | true | true | "SELECT * FROM users" | "SELECT * FROM users" | "267bb22fb46c39bf" | true
    nil | "active_record:sqlquery" | nil | nil | true | true | "SELECT 42" | "SELECT $1" | "50fde20626009aba" | true
    nil | "active_record:sqlquery" | nil | nil | true | true | "SELECT 42" | nil | nil | false
  end

  with_them do
    it_behaves_like "a tracing instrumenter" do
      let(:expected_span_name) { operation_name }
      let(:payload) { { name: name, exception: exception, connection_id: connection_id, cached: cached, sql: sql } }
      let(:expected_tags) do
        {
          "component" => "ActiveRecord",
          "span.kind" => "client",
          "db.type" => "sql",
          "db.connection_id" => connection_id,
          "db.cached" => cached_response,
          "db.statement" => expected_sql,
          "db.statement_fingerprint" => expected_fingerprint,
        }
      end

      let(:sampled) { tracing_sampled }
    end
  end
end
