# frozen_string_literal: true

require "spec_helper"
require_relative "../../../../support/tracing/shared_examples"

describe Labkit::Tracing::Rails::ActionView::RenderCollectionInstrumenter do
  using RSpec::Parameterized::TableSyntax

  where(:identifier, :count, :expected_count, :cache_hits, :expected_cache_hits, :exception) do
    nil | nil | 0 | nil | 0 | nil
    "" | nil | 0 | nil | 0 | nil
    "show.haml" | nil | 0 | nil | 0 | nil
    nil | 0 | 0 | nil | 0 | nil
    nil | 1 | 1 | nil | 0 | nil
    nil | nil | 0 | 0 | 0 | nil
    nil | nil | 0 | 1 | 1 | nil
    nil | nil | 0 | nil | 0 | StandardError.new
  end

  with_them do
    it_behaves_like "a tracing instrumenter" do
      let(:expected_span_name) { "render_collection" }
      let(:payload) { { exception: exception, identifier: identifier, count: count, cache_hits: cache_hits } }
      let(:expected_tags) do
        { "component" => "ActionView", "template.id" => identifier, "template.count" => expected_count, "template.cache.hits" => expected_cache_hits }
      end
    end
  end

  describe "#span_name" do
    context "when the template identifier is nil do" do
      before do
        allow(Labkit::Tracing::Rails::ActionView).to receive(:template_identifier).and_return(nil)
      end

      it "returns plain span name" do
        expect(
          described_class.new.span_name(
            identifier: "/Users/adam/projects/notifications/app/views/posts/_form.html.erb",
          )
        ).to eql("render_collection")
      end
    end

    context "when a template identifier is returned do" do
      before do
        allow(Labkit::Tracing::Rails::ActionView).to receive(:template_identifier).and_return(
          "app/views/hello.html.erb"
        )
      end

      it "returns plain span name" do
        expect(
          described_class.new.span_name(
            identifier: "/Users/adam/projects/notifications/app/views/posts/_form.html.erb",
          )
        ).to eql("render_collection:app/views/hello.html.erb")
      end
    end
  end
end
