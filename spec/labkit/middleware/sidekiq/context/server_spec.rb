# frozen_string_literal: true

require "sidekiq/testing"
require_relative "../../../../support/sidekiq_middleware/shared_contexts"

describe Labkit::Middleware::Sidekiq::Context::Server do
  class TestWorker
    include Sidekiq::Worker

    def perform(*args); end
  end

  include_context "with sidekiq server middleware setup"

  describe "wrapping jobs in application context" do
    let(:fake_job) do
      instance_double(TestWorker)
    end

    before do
      # Pushing the job when sidekiq is running inline sets the jid.
      allow(fake_job).to receive(:jid=)

      allow(TestWorker).to receive(:new).and_return(fake_job)
    end

    it "sets the application context from job params and clears after running" do
      expected_metadata = {
        "correlation_id" => "123",
        "meta.project" => "jane.doe/bookstore",
        "meta.user" => "jane.doe",
        "meta.random" => "key",
        :"meta.symbol" => "included",
      }
      job = expected_metadata.merge(
        "class" => "TestWorker",
        "queue" => "default",
        "args" => ["do it"],
        "something_else" => "not prefixed with meta",
        symbol: "not included", # rubocop: disable Style/HashSyntax explicitly testing mixed hash
      )

      expect(Labkit::Context).to receive(:with_context)
                                   .with(hash_including(expected_metadata.stringify_keys)).ordered.and_call_original
      expect(fake_job).to receive(:perform).with("do it").ordered

      Sidekiq::Client.push(job)
    end

    it "merges the caller_id to the params and pass it to the application context" do
      job = { "class" => "TestWorker", "queue" => "default", "args" => ["do it"] }

      expect(Labkit::Context).to receive(:with_context)
                                   .with(hash_including(Labkit::Context.log_key(:caller_id) => "TestWorker")).ordered.and_call_original
      expect(fake_job).to receive(:perform).with("do it").ordered

      Sidekiq::Client.push(job)
    end
  end
end
